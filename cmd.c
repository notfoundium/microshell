#include "shell.h"

t_cmd	*new_cmd()
{
	t_cmd	*cmd;

	cmd = (t_cmd*)ft_malloc(sizeof(t_cmd));
	cmd->argv = (char**)ft_calloc(1, sizeof(char*));
	cmd->next = NULL;
	cmd->size = 1;
	return (cmd);
}

t_cmd	*cmd_add(t_cmd *cmd)
{
	cmd->next = (t_cmd*)ft_malloc(sizeof(t_cmd));
	cmd = cmd->next;
	cmd->argv = (char**)ft_calloc(1, sizeof(char*));
	cmd->next = NULL;
	cmd->size = 1;
	return (cmd);
}

void	delete_cmd(t_cmd *cmd)
{
	size_t	i;
	t_cmd	*tmp;

	while (cmd)
	{
		i = 0;
		if (cmd->argv)
		{
			while (cmd->argv[i])
				free(cmd->argv[i++]);
			free(cmd->argv);
		}
		tmp = cmd;
		cmd = cmd->next;
		free(tmp);
	}
}
