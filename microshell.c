#include "shell.h"

void	argv_print(char **argv)
{
	int	i;

	i = 0;
	while (argv[i])
	{
		printf("[%s] ", argv[i]);
		i++;
	}
}

void	cmd_print(t_cmd *cmd)
{
	int	i;

	i = 0;
	while (cmd)
	{
		printf("{ ");
		argv_print(cmd->argv);
		printf("}\n");
		cmd = cmd->next;
	}
	printf("===\n");
}

void	child(char **argv, char **envp, int fd_in, int fd_out)
{
	if (fd_in != 0)
		dup2(fd_in, 0);
	if (fd_out != 1)
		dup2(fd_out, 1);
	execve(argv[0], argv, envp); // add error
	close(fd_in);
	close(fd_out);
	exit (0);
}

pid_t	exec(char **argv, char **envp, int fd_in, int fd_out)
{
	pid_t	pid;

	if (!strcmp(argv[0], "cd"))
		return (b_cd(argv));
	pid = fork();
	if (pid < 0)
		escape();
	if (pid == 0)
		child(argv, envp, fd_in, fd_out);
	if (fd_in != 0)
		close(fd_in);
	if (fd_out != 1)
		close(fd_out);
	return (pid);
}

void	cmd_exec(t_cmd *cmd, char **envp)
{
	int		i;
	int		j;
	pid_t	*arr_pid;
	int		fd[2];
	int		status;
	pid_t	pid;

	if (!cmd->argv[0])
		return ;
	arr_pid = (pid_t*)ft_calloc(1024, sizeof(pid_t));
	i = 0;
	while (cmd)
	{
		if (cmd->next)
		{
			pipe(fd);
			cmd->fd[1] = fd[1];
			cmd->next->fd[0] = fd[0];
		}
		pid = exec(cmd->argv, envp, cmd->fd[0], cmd->fd[1]);
		arr_pid[i++] = pid;
		cmd = cmd->next;
	}
	j = 0;
	while (j < i)
		waitpid(arr_pid[j++], &status, 0);
	free(arr_pid);
}

void	argv_pushback(t_cmd *cmd, const char *str)
{
	size_t	i;
	char	**tmp;

	i = 0;
	tmp = cmd->argv;
	cmd->argv = (char**)ft_calloc(++cmd->size, sizeof(char*));
	while (tmp[i])
	{
		cmd->argv[i] = tmp[i];
		i++;
	}
	cmd->argv[i] = ft_strdup(str);
	free(tmp);
}

t_cmd	*cmd_parse(int argc, char **argv)
{
	t_cmd		*cmd;
	t_cmd		*ret;
	static int	i = 1;

	if (i >= argc)
		return (NULL);
	cmd = new_cmd();
	ret = cmd;
	while (argv[i])
	{
		if (!strcmp(argv[i], ";"))
		{
			i++;
			return (ret);
		}
		else if (!strcmp(argv[i], "|"))
			cmd = cmd_add(cmd);
		else
			argv_pushback(cmd, argv[i]);
		i++;
	}
	return (ret);
}

int		main(int argc, char **argv, char **envp)
{
	int		i;
	t_cmd	*cmd;

	i = 1;
	(void)envp;
	if (argc > 1)
	{
		while (argv[i] && !strcmp(argv[i], ";"))
			i++;
		if (!argv[i])
			return (0);
		cmd = cmd_parse(argc, argv);
		while (cmd)
		{
			cmd_exec(cmd, envp);
			delete_cmd(cmd);
			cmd = cmd_parse(argc, argv);
		}
	}
	return (0);
}
