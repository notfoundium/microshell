#include "shell.h"

int		escape()
{
	write(2, "error: fatal\n", 13);
	exit(1);
	return (1);
}

size_t	ft_strlen(const char *str)
{
	size_t	len;

	len = 0;
	if (!str)
		return (0);
	while (str[len])
		len++;
	return (len);
}

char	*ft_strdup(const char *s)
{
	char	*newstr;
	size_t	len;
	size_t	i;

	len = ft_strlen(s);
	newstr = (char*)ft_calloc(len + 1, sizeof(char));
	i = 0;
	while (s[i])
	{
		newstr[i] = s[i];
		i++;
	}
	return (newstr);
}

void	*ft_malloc(size_t size)
{
	void	*p;

	p = malloc(size);
	if (!p)
		escape();
	return (p);
}

void	*ft_calloc(size_t nmemb, size_t size)
{
	char	*p;
	size_t	i;

	p = (char*)malloc(nmemb * size);
	i = 0;
	while (i < nmemb * size)
		p[i++] = 0;
	return (p);
}

int		b_cd(char **argv)
{
	int	ret;
	int	i;

	ret = 0;
	i = 0;
	while (argv[i])
		i++;
	if (i > 2)
	{
		write(2, "error: cd: bad arguments\n", 25);
		return (1);
	}
	ret = chdir(argv[1]);
	if (ret)
	{
		write(2, "error: cd: cannot change directory to ", 38);
		write(2, argv[1], ft_strlen(argv[1]));
		write(2, "\n", 1);
	}
	return (ret);
}
