#ifndef SHELL_H
# define SHELL_H

# include <stdlib.h>
# include <unistd.h>
# include <string.h>
# include <sys/types.h>
# include <sys/wait.h>

# include <stdio.h>

typedef struct		s_cmd
{
	char			**argv;
	size_t			size;
	int				fd[2];
	struct s_cmd	*next;
}					t_cmd;

size_t	ft_strlen(const char *str);
void	*ft_malloc(size_t size);
void	*ft_calloc(size_t nmemb, size_t size);
char	*ft_strdup(const char *s);
int		escape();

t_cmd	*new_cmd();
t_cmd	*cmd_add(t_cmd *cmd);
void	delete_cmd(t_cmd *cmd);

int		b_cd(char **argv);

#endif
